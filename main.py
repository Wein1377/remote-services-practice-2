import os
import psycopg2
from psycopg2 import sql
from tabulate import tabulate

url = os.environ.get('POSTGRES_URI')

print('Результат:')

try:
    connection = psycopg2.connect(url)
    cursor = connection.cursor()

    query = sql.SQL("""SELECT full_name, birthday FROM students ORDER BY birthday;""")

    cursor.execute(query)
    result = cursor.fetchall()

    if result:
        headers = ["ФИО", "Дата рождения"]
        print(tabulate(result, headers=headers, tablefmt="grid"))
    else:
        print('Ошибка')

    connection.close()

except Exception as e:
    print(e)
